# 【深度优先学习法】OpenEuler 安装配置 MySQL 过程涉及的 Linux 细节大起底

OpenEuler 中安装配置 MySQL 与其他系统有所不同, 需要一些手动的配置. 本文整理了在这个过程中涉及的一些 Linux 基础知识 

- [【深度优先学习法】OpenEuler 安装配置 MySQL 过程涉及的 Linux 细节大起底](#深度优先学习法openeuler-安装配置-mysql-过程涉及的-linux-细节大起底)
  - [只想看结果 :point_down:](#只想看结果-point_down)
  - [正文开始 :point_down:](#正文开始-point_down)
      - [修改配置文件： `sed` 指令](#修改配置文件-sed-指令)
      - [暂时禁用安全策略：SELinux](#暂时禁用安全策略selinux)
      - [MySQL 服务的本质：Linux Run Level 及其控制 与 自启动](#mysql-服务的本质linux-run-level-及其控制-与-自启动)

## 只想看结果 :point_down:

已验证的方法: [OpenEuler上MySQL的部署与使用_albert-rabbit的博客-CSDN博客](https://blog.csdn.net/weixin_43214408/article/details/116895091)

## 正文开始 :point_down:

#### 修改配置文件： `sed` 指令

在安装 MySQL 之前需要禁用 SELinux。SELinux 是 Linux 内核的安全策略, 如不禁用可能因为会访问权限的限制导致最后初始化时如下错误:

```shell
[root@host-x-x init.d]# mysqld --defaults-file=/etc/my.cnf –initialize
2021-08-10T07:08:26.745109Z 0 [System] [MY-010116] [Server] /usr/local/mysql/bin/mysqld (mysqld 8.0.22) starting as process 2916
2021-08-10T07:08:26.798812Z 1 [ERROR] [MY-011011] [Server] Failed to find valid data directory.
2021-08-10T07:08:26.798989Z 0 [ERROR] [MY-010020] [Server] Data Dictionary initialization failed.
2021-08-10T07:08:26.799066Z 0 [ERROR] [MY-010119] [Server] Aborting
2021-08-10T07:08:26.799407Z 0 [System] [MY-010910] [Server] /usr/local/mysql/bin/mysqld: Shutdown complete (mysqld 8.0.22)  Source distribution.
```

方法为:

```bash
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```


`sed` 用于对输入到该指令的文件进行处理并输出, 因此 `sed` 的参数字符串都是为了描述如何处理传入的文件

要理解 `sed` 指令, 需要知道, `sed` 指令的使用包括两个部分:**参数 flag** 和 **动作**

[参考](https://www.runoob.com/linux/linux-comm-sed.html)

- 参数 flag: 
  - `-e` 或 `-f` 之后的内容为描述如何处理文件的字符串. 不同的是使用 `-f` 时, 传入的是文件
  - **`-i` 直接对文件本身进行修改**
- 动作: 文件处理方法字符串的语法, (和 vim 的语法很像?
  - a ：新增， a 的后面可以接字串，而这些字串会在新的一行出现(目前的下一行)

    ```bash
    $ sed -e 4a\newline testfile #使用sed 在第四行后添加新字符串  
    ```

  - c ：取代， c 的后面可以接字串，这些字串可以取代 n1,n2 之间的行！

    ```bash
    [root@www ~]# nl /etc/passwd | sed '2,5c No 2-5 number'
    1 root:x:0:0:root:/root:/bin/bash
    No 2-5 number
    6 sync:x:5:0:sync:/sbin:/bin/sync
    .....(后面省略).....
    ```

  - d ：删除

    ```bash
    [root@www ~]# nl /etc/passwd | sed '2,5d'
    1 root:x:0:0:root:/root:/bin/bash
    6 sync:x:5:0:sync:/sbin:/bin/sync
    7 shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
    .....(后面省略).....
    ```

  - i ：插入， i 的后面可以接字串，而这些字串会在新的一行出现(目前的上一行)；
  - p ：打印，亦即将某个选择的数据印出。通常 p 会与参数 sed -n 一起运行～

    ```bash
    nl /etc/passwd | sed '/root/p' # 搜索'root'并展示
    1  root:x:0:0:root:/root:/bin/bash
    1  root:x:0:0:root:/root:/bin/bash
    2  daemon:x:1:1:daemon:/usr/sbin:/bin/sh
    3  bin:x:2:2:bin:/bin:/bin/sh
    4  sys:x:3:3:sys:/dev:/bin/sh
    5  sync:x:4:65534:sync:/bin:/bin/sync
    ....下面忽略 
    ```

  - s ：取代，可以直接进行取代的工作哩！通常这个 s 的动作可以搭配正规表示法！例如 1,20s/old/new/g 就是啦！

    ```bash
    sed 's/要被取代的字串/新的字串/g'
    ```

#### 暂时禁用安全策略：SELinux

[参考](https://zhuanlan.zhihu.com/p/86813709)

简而言之, SELinux 是 一个 Mandatory Access Control (MAC) 的实现, 建立在 LSM (Linux Security Module) 基础上, 参考了 Flask 的架构设计.

- **MAC**: 进程的权限控制并不是简单的 **谁可以做什么**, 而是**检查 进程 和 其请求的资源 的 context , 进而给出相应的安全策略**.
- Flask是一种灵活的操作系统安全架构，并且在Fluke research operating system中得到了实现。Flask的主要特点是把安全策略执行代码和安全策略决策代码，划分成了两个组件。

![](https://pic3.zhimg.com/80/v2-b1a6116c5d6a8025c7c1fff7abe5e196_1440w.jpg)

- LSM 在内核数据结构中增加了安全字段，并且在重要的内核代码（系统调用）中增加了hook。可以在hook中注册回调函数对安全字段进行管理，以及执行接入控制。

![](https://pic4.zhimg.com/80/v2-223cde664509658132da7b937d21d6fb_1440w.jpg)

SELinux 有 三个运行状态:

- Disable： 禁用SELinux，不会给任何新资源打Label，如果重新启用的话，将会给资源重新打上Lable，过程会比较缓慢。
- Permissive：如果违反安全策略，并不会真正的执行拒绝操作，替代的方式是记录一条log信息。
- Enforcing: 默认模式，SELinux的正常状态，会实际禁用违反策略的操作

使用 `getenforce` `setenforce [0|1]` 设置SeLinux 的运行状态.

#### MySQL 服务的本质：Linux Run Level 及其控制 与 自启动

MySQL 最终将作为一个 "**服务**" 在系统中运行, 并在 开机后 自动启动, 那么什么是 linux 中的 **服务**? 如何实现自启动?



在 Linux 中, `/etc/init.d/` (或 `/etc/rc.d/init.d`) 目录下有启动脚本, 一般称之为 **服务**. 

另一方面, Linux 将系统的运行生命周期抽象为 7 个状态:

- 0 – System halt i.e the system can be safely powered off with no activity.
- 1 – Single user mode.
- 2 – Multiple user mode with no NFS(network file system).
- 3 – Multiple user mode under the command line interface and not under the graphical user interface.
- 4 – User-definable.
- 5 – Multiple user mode under GUI (graphical user interface) and this is the standard runlevel for most of the LINUX based systems.
- 6 – Reboot which is used to restart the system.

在 `/etc/rc.d` 下有7个名为 `rcN.d` 的目录，对应系统的7个运行级别，其中存储的是一些指向软连接文件（类似 windows 中的快捷方式）

![](openeuler-mysql/2021-08-10-20-51-13.png)


[参考](https://www.cnblogs.com/yingsong/p/6012180.html)
