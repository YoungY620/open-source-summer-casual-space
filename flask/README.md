# Study Flask

- [Study Flask](#study-flask)
  - [view function构建 (对应 java 中 Controller func)](#view-function构建-对应-java-中-controller-func)
    - [route 映射](#route-映射)
    - [动态 url building: `url_for()`](#动态-url-building-url_for)
    - [用模板渲染: `render_template()`](#用模板渲染-render_template)
    - [Request](#request)
      - [全局 Request 实例获取](#全局-request-实例获取)
      - [Request Method (GET, POST, PUT, DELETE...)](#request-method-get-post-put-delete)
    - [Response](#response)
    - [cookies](#cookies)
    - [重定向 和 错误](#重定向-和-错误)

## view function构建 (对应 java 中 Controller func)

### route 映射

- 基本原型:

    ```python
    app = Flask(__name__)

    @app.route('/user/<string:username>')
    def profile(username):
        return f'{escape(username)}\'s profile'
    ```

  - 使用 `Flask` 之前, 需要通过 `app=Flask(<import_name>)` **获得Flask 实例**
  - 使用 `@app.route(...)` 将 **url 映射** 到控制函数. (可见与 spring 不同, 这里是面向过程的)
  - 路径中参数 `@app.route('/.../<param_type:param_name>')`, **注意 `param_name` 要与相应的 python 函数参数表相同**
  - 返回值将利用**模板引擎**, 自动转化为 `html` 渲染
  - 返回内容中使用 `escape()` 包裹参数 (建议), **防止 html 注入**. 例如 `name='alert("bad")'`, 只会显示文字`'alert("bad")'`, 不会执行 `alert`

### 动态 url building: `url_for()`

- 通过函数名得到静态url:

    ```py
    @app.route('/user/<username>')
    def profile(username):
        return f'{username}\'s profile'

    with app.test_request_context():
      print(url_for('profile', username='John Doe'))
    
    # /user/John%20Doe
    ```

- 静态资源: 注意静态 endpoint `'static'` 不能被重写

    ```py
    url_for('static', filename='style.css')
    ```

### 用模板渲染: `render_template()`

```py
@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)
```

静态模板放在与 `app.py` 同级的 `/templates/` 中

```html
<!doctype html>
<title>Hello from Flask</title>
{% if name %}
  <h1>Hello {{ name }}!</h1>
{% else %}
  <h1>Hello, World!</h1>
{% endif %}
```

### Request

#### 全局 Request 实例获取

...

#### Request Method (GET, POST, PUT, DELETE...)

- 对于路径内 (GET) 参数 `..?key1=v1&key2=v2`, `request.args` 是一个 `MultiDict[str, str]` 类型

    ```python
    searchword = request.args.get('key1', 'key2')
    ```

- 对于 POST 或 PUT (传 Object) 不同 `content-type` 方法不同:

    <details>
        <summary>unfold example</summary>

    ```py
    @app.route('/login', methods=['POST'])
    def login():
        rawdata = data = None
        if(request.content_type.startswith('application/json')):
            data = request.get_json()
            rawdata = request.get_data()
        elif(request.content_type.startswith('multipart/form-data')):
            data = request.form
        elif(request.content_type.startswith('application/x-www-form-urlencoded')):
            data = request.values
        print(data, data['a'], data['b'])
        print(rawdata)
        return "Get it!"
    ```
    </details>

### Response

- view function 的返回值会[根据其类型自动包裹为 response body](https://flask.palletsprojects.com/en/2.0.x/quickstart/#about-responses). 如上文字符串自动以 txt/html 格式, `200 OK` 状态码发送 response
- 手动控制 response body, 获取 response 对象实例: `make_response()`

### cookies

- Cookies 是什么? 
  - http 协议是无状态的, cookies 存储在用户端硬件上, 用于存储一些 "状态", 以实现一些状态逻辑
  - 本质是一个应用端自定义的表
- 通过 request 获取, 通过 response 设置, 发送
- [`Session` 是一种更安全的封装](https://flask.palletsprojects.com/en/2.0.x/quickstart/#sessions) (?)

### 重定向 和 错误

- `redirect()`
- `@app.errorhandler(401)`
