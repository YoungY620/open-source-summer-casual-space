from flask import Flask
from flask import url_for
from flask import render_template
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    return 'index'

# @app.route('/login')
# def login():
#     return 'login'

@app.route('/user/<username>')
def profile(username):
    return f'{username}\'s profile'

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)

@app.route('/login', methods=['POST'])
def login():
    rawdata = data = None
    if(request.content_type.startswith('application/json')):
        data = request.get_json()
        rawdata = request.get_data()
    elif(request.content_type.startswith('multipart/form-data')):
        data = request.form
    elif(request.content_type.startswith('application/x-www-form-urlencoded')):
        data = request.values
    print(data, data['a'], data['b'])
    print(rawdata)
    print(request)
    return data

if __name__ == '__main__':
    with app.test_request_context():
        print(url_for('index'))
        print(url_for('login'))
        print(url_for('login', next='/'))
        print(url_for('profile', username='John Doe'))
