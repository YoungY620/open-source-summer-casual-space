# Flask Tutorial Example

- **启动函数为什么是 `create_app`**
  - flask 框架启动时自动检测名为 `create_app` 或 `make_app` 的工厂函数
  - 在函数中创建 `flask app` 实例, 灵活?

- **flask 多文件整合**:
  - 蓝图:
    - (一般) 在一个文件中定义一个蓝图, 在该文件中, 蓝图与单文件应用中的 flask 实例用法相同
    - 类似于 spring mvc 中的 Controller, 可以定义全局的 url prefix
    - 在 `create_app` 中注册所有蓝图即可.

- **数据库**
  - (在该 tutorial 中) 每次使用时, `get_db` 得到实例, `execute` sql 语句