# 如何编写一个 Python Web 服务

最近在为一个开源社区开发一个 FAQ (Frequently Asked Questions) 服务模块, 开发语言我选择了 Python. 这也是我第一次使用 Python 编写 Web服务. 之前我一直习惯用 Java 来进行 Web 后端开发, 而我熟悉的一套最基础的 Java Web 技术栈是这样的:

- Spring X: 整个应用的框架, 这里的 'X' 包括 'boot', 'cloud', 等等
- Mybatis/Mybatis-plus: 数据库 ORM
- Mybatis-plus-generator: 逆向工程, 即由数据库生成 DAO. 但它能做的不止于此, 它还可以生成整个目录结构
- fastjson/gson/jackson: json 解析
- Maven/gradle: 依赖管理. 使用一个 `pom.xml`/`build.gradle` 文件使得项目可以快速构建环境

而与之对应的, Python 中的一套最基本的技术栈:

- Flask: 整个应用的框架
- Flask-SQLAlchemy: SQLAlchemy 在 flask 中的插件. 用于数据库 ORM. 相比 Java, 它提供了更灵活的接口, 尤其是对于复杂关系
- flask-sqlacodegen: 针对 SQLAlchemy 的逆向工程
- jsonschema: 用于 json 请求体的校验
- pip: 依赖管理. 对于 Web 开发来说, pip 已经足够了

**目录**:

- [如何编写一个 Python Web 服务](#如何编写一个-python-web-服务)
  - [Flask](#flask)
    - [Application Context](#application-context)
    - [View Functions & CLI Command](#view-functions--cli-command)
    - [Blueprints 蓝图](#blueprints-蓝图)
  - [Flask-SQLAlchemy](#flask-sqlalchemy)
    - [简单增删改查](#简单增删改查)
    - [复杂关系: 一对一, 一对多](#复杂关系-一对一-一对多)
    - [使用 sqlacodegen 实现针对 SQLAlchemy 的逆向工程](#使用-sqlacodegen-实现针对-sqlalchemy-的逆向工程)
  - [jsonschema: 复杂 json 请求体 (request body) 的校验](#jsonschema-复杂-json-请求体-request-body-的校验)
  - [效率提升: 使用 PowerShell 指令配置 Pycharm run configuration 实现自动逆向工程与自动更新依赖文件 requirements.txt](#效率提升-使用-powershell-指令配置-pycharm-run-configuration-实现自动逆向工程与自动更新依赖文件-requirementstxt)
    - [自动逆向工程](#自动逆向工程)
    - [自动更新配置文件](#自动更新配置文件)
    - [Pycharm run/debug configurations](#pycharm-rundebug-configurations)

## Flask

对于 Flask 最核心的有三点:

- **Application Context**: 将整个应用连成一体.
- **View Function** & **CLI Command**: 应用暴漏给外界的操作接口
- **Blueprints (蓝图)**: 实现模块化开发

### Application Context

<!-- 在 Spring boot 中, 有依赖注入, 控制反转, 一个类只要有相应的 bean, spring 就可以对他进行控制.  -->

<!-- 而 Flask 虽然更接近于函数式编程, 但个人理解, 其中的 **View Function** 和 **CLI Command** 某种程度上也是一种 (所谓的) "控制反转", 他们都注册在在一个 Application Context 之下接受调度, 你并不需要手动控制这些函数的生命周期 (调用, 获取返回值等) -->

参考: [Flask 2.0.x](https://flask.palletsprojects.com/en/2.0.x/#)

我更习惯使用 工厂模式 创建 Context: 在 `__init__.py` 中写 `create_app()`:

```python
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    configure(app, test_config)

    db.init_app(app)

    from faq.blueprints import review
    app.register_blueprint(review.bp)

    return app
```

当在命令行输入命令 `flask run` 时, flask 将自动寻找 `create_app` 函数来执行, 以创建 Application Context.

PS: `flask run` 便是一个默认的 `CLI Command`

默认在 View Function 和 CLI Command 中自动就在 当前应用的 Context 下. 而此外其他的场景下, 需要手动的创建/引入 Application Context. [flask-sqlalchemy 中的例子](https://flask-sqlalchemy.palletsprojects.com/en/2.x/contexts/): 

```python
with app.app_context():
    user = db.User(...)
    db.session.add(user)
    db.session.commit()
```

但实际上 有 View Function 和 CLI 就足够了

### View Functions & CLI Command

View Function 主要做到了路径和函数的映射绑定, 同时借助 Flask 其他的模块 (`request`, `current_app`, `make_response` 等), 获得参数和返回响应

参考: [Flask 2.0.x Doc](https://flask.palletsprojects.com/en/2.0.x/#)

这是一个 View Function 的例子. 

```python
bp = Blueprint('review_show', __name__, url_prefix='/review/show', cli_group=None)


@bp.route('/requests/<user_id>', methods=['GET'])
def show_requests(user_id):
    page = int(request.args.get('page'))
    per_page = int(request.args.get('page_size'))
    data: dict = request.get_json()
    print(data)

    current_app.logger.info("request user id: %s", user_id)

    requests: Pagination = ERequest.query \
        .order_by(ERequest.time.desc()) \
        .paginate(page=int(page), per_page=int(per_page), error_out=False)
    body = [RequestEntry(rq) for rq in requests.items]
    return make_response(jsonify(body), 200)
```

获得 param 参数:

```python
page = int(request.args.get('page'))
per_page = int(request.args.get('page_size'))
```

获得json 格式的 request body:

```python
data: dict = request.get_json()
print(data)
```

响应

```python
return make_response(jsonify(body), 200)
```

接下来是 CLI Command:

参考: 
- [custom-commands](https://flask.palletsprojects.com/en/2.0.x/cli/#custom-commands), 
- [testing-cli-commands: 包含了两种参数 (option, argument) 的用法](https://flask.palletsprojects.com/en/2.0.x/testing/#testing-cli-commands)

```python
bp = Blueprint('review_show', __name__, cli_group='cli')

@bp.cli.command('test')
@click.option('--who', default='tom')
@click.argument('what', default='good morning')
def hello(who, what):
    print(f'hello: {who},{what}')
```

运行:

```powershell
PS D:\my\flask\app> $env:FLASK_APP = "faq"
PS D:\my\flask\app> flask cli hello
hello: tom,good morning
PS D:\my\flask\app> flask cli hello goodby --who bob
hello: bob,goodby
PS D:\my\flask\app>
```

### Blueprints 蓝图

从 Java 转来的我十分希望实现像 Spring boot Controller 那样的多模块开发. 即将不同类型或不同需求模块的 API 放在不同的 Controller 中, 然后通过命名加以区分. 

Blueprints 就实现了这点. 蓝图可以注册到 app 中去, 同时又可以注册其他子蓝图, 从而可以实现一个树状的注册结构.

比如, 这是我的文件目录

```text
│  models.py
│  __init__.py
│
└─blueprints
   │  __init__.py
   │
   └─ review
     │  handle.py
     │  show.py
     └─ __init__.py

```

`/blueprints/show.py` 中 我定义了具体的 View Functions 和 CLI

```python
bp = Blueprint('review_show', __name__, url_prefix='/review/show', cli_group='cli')


@bp.cli.command('hello')
@click.option('--who', default='tom')
@click.argument('what', default='good morning')
def hello(who, what):
    print(f'hello: {who},{what}')


@bp.route('/requests/<user_id>', methods=['GET'])
def show_requests(user_id):
    pass
```

这个蓝图 `review_show` 注册到另一个蓝图 `review` 中:

`/blueprints/review/__init__.py`:

```python
from flask import Blueprint

from faq.blueprints.review import show, handle

bp = Blueprint('review', __name__)

bp.register_blueprint(show.bp)
bp.register_blueprint(handle.bp)
```

最终, 蓝图 `review` 注册到 app 中:

`/__init__.py`:

```python
def create_app(test_config=None):
    ......

    from faq.blueprints import review
    app.register_blueprint(review.bp)

    return app
```

## Flask-SQLAlchemy 

### 简单增删改查

参考:

- [flask-sqlalchemy: 增删查](https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/)
- [两种 update](https://blog.csdn.net/li944254211/article/details/109328074)

### 复杂关系: 一对一, 一对多

在 Mybatis 中, 实现一对多和一堆一, 需要在 .xml 文件或 `@Select` 注解中生命映射, 但咱 SQLAlchemy 中, 只需要一个参数.

这是一个例子. 这里 `EQuestion` 和 `EQuestionDescription` 是一对多的关系. 

`/models.py`:

```python
from faq import db


class EQuestion(db.Model):
    __tablename__ = 'e_question'

    id = db.Column(db.String(20), primary_key=True)
    std_description = db.Column(db.String(250), unique=True)


class EQuestionDescription(db.Model):
    __tablename__ = 'e_question_description'

    id = db.Column(db.String(20), primary_key=True)
    description = db.Column(db.String(200), nullable=False)
    question_id = db.Column(db.ForeignKey('e_question.id'), index=True)

    question = db.relationship('EQuestion', backref='e_question_descriptions', primaryjoin='EQuestionDescription.question_id == EQuestion.id')
```

只需要在 "一对多" 中 "多" 的一方加入:

```python
question = db.relationship('EQuestion', backref='e_question_descriptions', primaryjoin='EQuestionDescription.question_id == EQuestion.id')
```

接下来, 编写一个 CLI COmmand 来测试一下:

```python
@app.cli.command('hello')
def hello():
    question: EQuestion = EQuestion.query.get('2')
    print(question)
    
    descriptions = question.e_question_descriptions
    print(descriptions)
    
    description: EQuestionDescription = descriptions[0]
    print(description)
    
    question: EQuestion = description.question
    print(question)
```

结果:

```
PS D:\my\flask\app> flask hello
<EQuestion 2>
[<EQuestionDescription 3>, <EQuestionDescription 9>]
<EQuestionDescription 3>
<EQuestion 2>
```

### 使用 sqlacodegen 实现针对 SQLAlchemy 的逆向工程

这里只是介绍这个有力的工具:

参考: [flask 插件 flask-sqlacodegen](https://pypi.org/project/flask-sqlacodegen/)

需要注意的是, 由 flask-sqlacodegen 直接生成的代码并不能直接适应我们原有的代码

下面是在 PowerShell 中完整的生成命令. 他将在当前目录中生成一个 `models.py` 文件

```
PS D:\my\flask\app> (.\venv\Scripts\Activate.ps1) ;((flask-sqlacodegen mysql://root:123456@localhost:3306/openeuler_faq --flask) -replace 'db = SQLAlchemy\(\)','from faq import db' -replace 'from flask_sqlalchemy import SQLAlchemy','' | out-file faq/models.py -encoding utf8)
```

关于这一命令的含义, 我将在最后一节详谈

## jsonschema: 复杂 json 请求体 (request body) 的校验

这时我发现的一个强大的 json 数据校验工具, 不止可以用在 flask app 中 json 数据的校验, 在任何场景 json 数据的校验都非常有力

一般的, 数据校验包含这样几个层次:

- **格式校验**: 是否符合 json 语法
- **属性校验**: 对于 json 表示的对象 (object) 是否包含指定的属性, 是否包含了其他不需要的数据, 以及每种属性是否是规定的类型
- **值校验**: 对数据取值进行校验. 例如规定字符串的长度范围, 数字属性的取值范围, 集合属性的元素个数和元素的数据类型
- **逻辑校验**: 这就和业务逻辑相关了. 比如传入的 id 指向的用户是否有操作的权限. 传入的数据是否与数据库中已有的数据冲突等. 这一块就很难使用框架实现了.

首先, 什么是 JSON Schema? 可以参考下面的资料: 

- 快速开始: [getting-started-step-by-step](https://json-schema.org/learn/getting-started-step-by-step)
- [understanding json schema](http://json-schema.org/understanding-json-schema/)
- [understanding json schema 中文翻译](https://json-schema.apifox.cn/)

jsonschema 及其衍生的工具生态除了提供上述功能外, 还其他提升易用性的工具:

- **jsonschema** (python module): 为数据校验提供了一个 SDK, 提供了校验接口, 和详尽的错误提示功能
- **json schema Tool**: 一个在线的 json schema 生成与图形化编辑工具, 帮助你写出符合语法的 json schema 规约文件. [网址](https://www.jsonschema.net/home)

下面是一个涵盖大部分用例的 jsonschema python module 使用案例([参考](https://www.cnblogs.com/c-keke/p/14888876.html))

```python
from jsonschema import validate, ValidationError # 导入参数的包

@app.route('/login4', methods=['POST'])
def login4():
    body = request.get_json()
    try:
        validate(
            body,
            {
                "$schema": "http://json-schema.org/learn/getting-started-step-by-step",
                # 描述对应的JSON元素，title相对来说，更加简洁
                "title": "book info",
                # 描述对应的JSON元素，description更加倾向于详细描述相关信息
                "description": "some information about book",
                # 该关键字用于限定待校验JSON元素所属的数据类型，取值可为：object，array，integer，number，string，boolean，null
                "type": "object",
                # 用于指定JSON对象中的各种不同key应该满足的校验逻辑，
                # 如果待校验JSON对象中所有值都能够通过该关键字值中定义的对应key的校验逻辑，每个key对应的值，都是一个JSON Schema，则待校验JSON对象通过校验。
                "properties": {
                    "id": {
                        "description": "The unique identifier for a book",
                        "type": "integer",
                        "minimum": 1
                    },
                    "name": {
                        "description": "book name",
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 30
                    },
                    "tips": {
                        "anyOf": [  # 满足其中一个类型 就行
                            {"type": "string", "minLength": 10, "maxLength": 60}, 
                            {"type": "number", "minimum": 5.0}
                        ]
                    },
                    "price": {
                        "description": "book price",
                        "type": "number",
                        # 能被0.5整除
                        "multipleOf": 0.5,
                        # 这里取等，5.0=<price<=99999.0
                        "minimum": 5.0,
                        "maximum": 99999.0,
                        # 若使用下面这两个关键字则 5.0<price<99999.0
                        # "exclusiveMinimum": 5.0,
                        # "exclusiveMaximum": 99999.0
                    },
                    "tags": {
                        "type": "array",
                        "items": [
                            {
                                "type": "string",
                                "minLength": 2,
                                "maxLength": 8
                            },
                            {
                                "type": "number",
                                "minimum": 1.0
                            }
                        ],
                        # 待校验JSON数组第一个元素是string类型，且可接受的最短长度为5个字符，第二个元素是number类型，且可接受的最小值为10
                        # 剩余的其他元素是string类型，且可接受的最短长度为2。
                        "additonalItems": {
                            "type": "string",
                            "miniLength": 2
                        },
                        # 至少一个
                        "miniItems": 1,
                        # 最多5个
                        "maxItems": 5,
                        # 值为true时，所有元素都具有唯一性时，才能通过校验。
                        "uniqueItems": True
                    },
                    "date": {
                        "description": "书籍出版日期",
                        "type": "string",
                        # 可以是以下取值：date、date-time（时间格式）、email（邮件格式）、hostname（网站地址格式）、ipv4、ipv6、uri等。
                        # 使用format关键字时，在实例化validator时必须给它传format_checker参数，值如：draft7_format_checker, 网址：
                        # https://python-jsonschema.readthedocs.io/en/latest/validate/#jsonschema.Draft7Validator
                        "format": "date",
                    },
                    "bookcoding": {
                        "description": "书籍编码",
                        "type": "string",
                        # 符合该关键字指定的正则表达式，才算通过校验。
                        "pattern": "^[A-Z]+[a-zA-Z0-9]{12}$"
                    },
                    "other": {
                        "description": "其他信息",
                        "type": "object",
                        "properties": {
                            "info1": {
                                "type": "string"
                            },
                            "info2": {
                                "type": "string"
                            }
                        }
                    }
                },
                # 指定了待校验JSON对象可以接受的最少 一级key 的个数
                "minProperties": 3,
                # 指定了待校验JSON对象可以接受的最多 一级key 的个数。
                "maxProperties": 7,
                # patternProperties对象的每一个一级key都是一个正则表达式，value都是一个JSON Schema。
                # 只有待校验JSON对象中的一级key，通过与之匹配的patternProperties中的一级正则表达式，对应的JSON Schema的校验，才算通过校验。
                # 下面的JSON Schema表示, 所有以a开头的一级key的value都必须是number，
                "patternProperties": {
                    "^a": {
                        "type": "number"
                    },
                },
                # 如果待校验JSON对象中存在，既没有在properties中被定义，又没有在patternProperties中被定义，那么这些一级key必须通过additionalProperties的校验。
                "additionalProperties": {
                    "desc": {
                        "type": "string",
                        "minLength": 1
                    },
                },
                # 该关键字限制了JSON对象中必须包含哪些一级key。
                # 如果一个JSON对象中含有required关键字所指定的所有一级key，则该JSON对象能够通过校验。
                "required": ["id", "name", "info", "price"]
            })
    
    except ValidationError as e:
        msg = "json数据不符合schema规定：\n出错字段：{}\n提示信息：{}".format(" --> ".join([i for i in e.path]), e.message)
        print(msg)
        return jsonify(status=500, msg=msg)
        
    print(body)
    title = body.get('title')
    return '1'
```

再加一个使用 **枚举** 的例子. 枚举指定了属性可以取哪些值.  

参考: [enum 的文档](http://json-schema.org/understanding-json-schema/reference/generic.html#enumerated-values)

```python
def check_request(user_id, req_body):
    if request.is_json:
        handle_request_schema = {
            "title": "handle requests",
            "type": "object",
            "properties": {
                "id": {"type": "string", "maxLength": 20},
                "tags": {
                    "type": "array",
                    "items": {"enum": [tag.tag_name for tag in ETag.query.all()]},
                    "uniqueItems": True
                },
                "self_answers": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "id": {"type": "string", "maxLength": 20},
                            "type": {"enum": [elem.type_name for elem in CAnswerType.query.all()]},
                            "level": {"enum": [elem.level for elem in CAnswerLevel.query.all()]}
                        },
                        "required": ["id", "allowed", "comment", "author_id",
                                     "type", "content", "summary", "level"]
                    }
                },
                "adjusted_answers": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "id": {"type": "string", "maxLength": 20},
                            "level": {
                                "enum": [level.level for level in CAnswerLevel.query.all()]}
                        },
                    },
                }
            }
        }
        try:
            validate(req_body, handle_request_schema)
        except ValidationError as e:
            msg = "json数据不符合schema规定：\n出错字段：{}\n提示信息：{}".format(".".join([str(i) for i in e.path]), e.message)
            return msg, 500
    else:
        return "请求体必须是JSON格式", 500
    return "", 200
```

值得一提的是, 在上面这个例子中, 枚举取值是在运行时动态加载的, 这给程序的编写和维护提供了很大的便利性

不仅是枚举, 实际上字段的长度也可以通过 SQLAlchemy 动态获取: 

[方法: how-to-get-sqlalchemy-length-of-a-string-column](https://stackoverflow.com/questions/1777814/how-to-get-sqlalchemy-length-of-a-string-column)

## 效率提升: 使用 PowerShell 指令配置 Pycharm run configuration 实现自动逆向工程与自动更新依赖文件 requirements.txt

### 自动逆向工程

前面提到逆向工程代码生成的指令为:

([前文链接](#使用-sqlacodegen-实现针对-SQLAlchemy-的逆向工程))

```
PS D:\my\flask\app> (.\venv\Scripts\Activate.ps1) ;((flask-sqlacodegen mysql://root:123456@localhost:3306/openeuler_faq --flask) -replace 'db = SQLAlchemy\(\)','from faq import db' -replace 'from flask_sqlalchemy import SQLAlchemy','' | out-file faq/models.py -encoding utf8)
```

这一 PowerShell 中 `;` 代表指令顺序执行, 相当于 `&&` (Linux). `-replace` 对生成内容做了替换. `out-fule` 指定了输出文件名, 同时规定了编码: utf-8

### 自动更新配置文件

pip 通过 `requirements.txt` 指定了程序所有的依赖, 而 `pip freeze` 指令可以输出当前全部的依赖

完整代码:

```
(.\venv\Scripts\Activate.ps1) ; (python -m pip freeze > requirements.txt)
```

### Pycharm run/debug configurations

依次点击:

![](2021-08-19-16-09-04.png)

添加一个 shell script, 将指令粘贴到标注位置即可

![](2021-08-19-16-43-06.png)
