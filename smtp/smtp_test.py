import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr, parseaddr
from pyparsing import unicode

 
# 第三方 SMTP 服务
mail_host="smtp.QQ.com"  #设置服务器
mail_user="1394735766@qq.com"    #用户名
 

sender = '1394735766@qq.com'
receivers = ['youngyee620@gmail.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
 
message = MIMEText('Python 邮件发送测试...', 'plain', 'utf-8')
name, addr = parseaddr("系统自动发送 <1394735766@qq.com>")
message['From'] = formataddr((Header(name, 'utf-8').encode(), addr if isinstance(addr, unicode) else addr))
message['To'] =  Header("测试", 'utf-8')

subject = 'Python SMTP 邮件测试'
message['Subject'] = Header(subject, 'utf-8')
 
 
try:
    smtpObj = smtplib.SMTP() 
    smtpObj.connect(mail_host, 25)    # 25 为 SMTP 端口号
    smtpObj.login(mail_user,mail_pass)  
    smtpObj.sendmail(sender, receivers, message.as_string())
    print ("success")
except smtplib.SMTPException:
    print ("Error: 无法发送邮件")